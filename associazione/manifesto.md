---
title:  HackLab Cormano - Manifesto
author: atrent&gio
geometry: margin=3cm,paper=a4paper
fontsize: 12pt
date: Aprile 2016
---

Riconosciamo nell'informatica e nell'elettronica strumenti essenziali per la **partecipazione attiva** di ciascun individuo alla vita della propria comunità: affinché siano autentici fattori di sviluppo artistico, culturale, economico e politico, riteniamo necessario che la loro conoscenza e pratica non restino sapere di pochi ma diventino **patrimonio diffuso**, così che ciascuno abbia coscienza del rapporto fra tecnologia e cittadinanza.

Per perseguire questo scopo intendiamo promuovere la più ampia diffusione dell'informatica e dell'elettronica come espressione della filosofia del [**Software Libero**](https://www.gnu.org/philosophy/free-sw.it.html) e del [**Hardware Libero**](http://www.oshwa.org/definition/Italian/), che garantiscono a ciascuno la libertà di poter contribuire alla propria comunità accedendo, creando, modificando, pubblicando e distribuendo le [**Opere Culturali Libere**](http://freedomdefined.org/Definition/It) collettivamente sviluppate.

In particolare, le libertà che intendiamo promuovere con la nostra associazione sono:

0. la libertà di **usare** l'opera e di goderne i benefici derivanti dall'uso
1. la libertà di **studiare** l'opera e di impiegare la conoscenza acquisita da essa
2. la libertà di **creare e ridistribuire copie**, in tutto o in parte, dell'informazione o espressione
3. la libertà di **fare modifiche e miglioramenti**, e di distribuire opere derivate

Riconosciamo inoltre nella [**Cultura Hacker**](https://en.wikipedia.org/wiki/Hacker_culture) un costruttivo approccio di partecipazione alla nostra società e intendiamo dare il nostro contributo alla conoscenza e diffusione del significato originale dei termini [*hacker*](https://www.cs.berkeley.edu/~bh/hacker.html), *hacking* e [*hacklab*](https://en.wikipedia.org/wiki/Hackerspace): termini che indicano la curiosità verso il funzionamento dei sistemi, la voglia di condividerne la conoscenza e di partecipare al loro miglioramento.

Intendiamo perseguire i nostri scopi tramite:

* organizzazione di un *hacklab* aperto a tutti i soci;
* pubblicazione e distribuzione di materiale informativo, formativo, critico, saggistico sotto forma di *Opere Culturali Libere*;
* utilizzo, studio, sviluppo, distribuzione e pubblicazione di *Software Libero*;
* utilizzo, studio, sviluppo e produzione di *Hardware Libero*;
* organizzazione di convegni e seminari;
* utilizzo dei mezzi di comunicazione radio e televisivi e delle reti informatiche in maniera funzionale ai nostri scopi;
* collaborazione nei confronti di qualsiasi persona fisica, giuridica o realtà istituzionale che lo richiedesse, in conformità ai nostri scopi.
 
Svolgiamo la nostra attività in totale autonomia finanziaria rispetto agli associati e ad ogni altro ente, persona fisica, persona giuridica o realtà istituzionale con i quali collaboriamo.

Intendiamo **favorire la partecipazione** di tutte le persone e a questo scopo dichiariamo che:

* l'impegno economico dovrà essere stabilito in modo tale da mantenere l'associazione accessibile a tutte le persone;
* devono essere previste forme di partecipazione alle assemblee e - possibilmente - di votazione via web. La partecipazione all'associazione non dovrebbe obbligare ad una presenza fisica alle riunioni ed alle iniziative organizzate;
* non devono essere posti ostacoli all'ingresso di nuovi soci, quali approvazioni da parte di organi dell'Associazione o presentazioni da parte di altri associati, fermo restando l'impegno dei soci di rispettare il presente manifesto lo statuto.

L'Associazione è apartitica: l'esercizio e la manifestazione della propria appartenenza a partiti o movimenti politici dovrà avvenire in altre e separate sedi.

**L'Associazione è politica**: la partecipazione attiva alla vita della propria comunità secondo i principi espressi in questo manifesto è di per sé attività politica.
