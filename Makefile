######################################################
# Makefile for HackLab Cormano website and documents 
######################################################

# FIXME: completely missing dependency checks, ala make configure (to check config)

# Settings
# #######################################

PANDOC = pandoc
STATIC = static_output
ASSOCIAZIONE = associazione

dir_guard=@mkdir -p $(@D)

.PHONY: help staticdir clean deploy

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  manifesto          build our manifesto (PDF)"
	@echo "  statuto            build our statuto (PDF and Markdown)"
	@echo "  attocostitutivo    build our atto costitutivo (PDF and Markdown)"
	@echo "  associazione       build all above documents"
	@echo "  "
	@echo "  clean              clean documents"
	@echo "  deploy             deploy documents"


# All associazione documents
associazione: manifesto statuto attocostitutivo

# Manifesto
manifesto: $(ASSOCIAZIONE)/$(STATIC)/manifesto.pdf

$(ASSOCIAZIONE)/$(STATIC)/manifesto.pdf: $(ASSOCIAZIONE)/manifesto.md
	$(dir_guard)
	pandoc $< -o $@ -V links-as-notes

# Statuto
statuto: $(ASSOCIAZIONE)/$(STATIC)/statuto.pdf $(ASSOCIAZIONE)/$(STATIC)/statuto.md

$(ASSOCIAZIONE)/$(STATIC)/statuto.pdf: $(ASSOCIAZIONE)/statuto.tex
	$(dir_guard)
	pdflatex -output-directory $(@D) $< $(@F)

$(ASSOCIAZIONE)/$(STATIC)/statuto.md: $(ASSOCIAZIONE)/statuto.tex
	$(dir_guard)
	pandoc $< -o $@

# Atto costitutivo
attocostitutivo: $(ASSOCIAZIONE)/$(STATIC)/atto_costitutivo.pdf $(ASSOCIAZIONE)/$(STATIC)/atto_costitutivo.md

$(ASSOCIAZIONE)/$(STATIC)/atto_costitutivo.pdf: $(ASSOCIAZIONE)/atto_costitutivo.tex
	$(dir_guard)
	pdflatex -output-directory $(@D) $< $(@F)

$(ASSOCIAZIONE)/$(STATIC)/atto_costitutivo.md: $(ASSOCIAZIONE)/atto_costitutivo.tex
	$(dir_guard)
	pandoc $< -o $@

####################
# maintenance tasks
clean:
	rm -R $(ASSOCIAZIONE)/$(STATIC)/*

deploy:
	@echo "To be done..."

# FIXME: probably make can make this with just one parametrized stanza for all check-<command> needed
check-pandoc:
ifeq ($(shell which $(PANDOC) >/dev/null 2>&1; echo $$?), 1)
	$(error '$(PANDOC)' not found. Please make sure you have it installed and the full path of the executable is in your PATH.')
endif
